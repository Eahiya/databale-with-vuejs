# databale-with-vuejs


        async get_upcoming_article(day) {
            /* if (day == this.selectedDay && this.upcoming_article.length > 0) {
                return;
            }*/
            let data = {
                day: day,
            };
            this.selectedDay = day;
            // const res = await this.callApi("post", "sage-writer", data);
            // if (res) {
            //     await this.$store.commit(
            //         "article/SET_ALL_UPCOMING_ARTICLES",
            //         res.data.data
            //     );
            // }

            let sageWriterTable = $("#sage-writer-table").DataTable({
                destroy: true,
                responsive: true,
                searching: true,s
                paging: true,
                pageLength: 10,
                serverSide: true,
                ajax: {
                    url: "https://sage-tqdk23d5hq-uc.a.run.app/api/sage-writer",
                    type: "post",
                    headers: {
                        Authorization:
                            "Bearer 14|QZATJvKQ1dJSW47GBMP37eJoFGGEcbeTwDOvBi3Va9d3e51f", // Replace with your actual authorization token
                    },
                    data: function (d) {
                        return {
                            day,
                            page: Math.floor(d.start / d.length) + 1,
                            perpage: d.length,
                        };
                    },
                    dataFilter: function (data) {
                        var json = jQuery.parseJSON(data);
                        json.recordsTotal = json.meta.total;
                        json.recordsFiltered = json.meta.total;
                        json.data = json.data;
                        return JSON.stringify(json);
                    },
                    dataSrc: (response) => {
                        // this.upcoming_article = response.data;
                        return response.data;
                    },
                },
                language: {
                    processing: "Loading data...", // Customize the processing text
                },
                columns: [
                    {
                        data: "published_date",
                    },
                    {
                        data: "title",
                    },
                    {
                        data: "keyword_status",
                        render: function (data, type, row, meta) {
                            if (type === "display") {
                                let domData;
                                if (row.total_article > 0) {
                                    domData = `<a
                                                href="/keyword-report/?id=${row.id}"
                                                class="text-[#172B4D] text-[12px] font-medium underline"
                                                >View Report
                                            </a>`;
                                } else if (
                                    row.requestSubmitted == 1 &&
                                    row.total_article == 0
                                ) {
                                    domData = ` <span
                                                class="badge-btn bg-yellow-400 text-black w-75 text-center"
                                            >
                                                Keyword Request Submitted
                                            </span>`;
                                } else if (
                                    row.requestSubmitted == 0 &&
                                    row.total_article == 0
                                ) {
                                    domData = `<button
                                                @click="
                                                    automatedKeywordFlowDialog(
                                                        ${row}
                                                    )
                                                "
                                                class="flex text-[#0052CC] font-medium space-x-2 text-[12px]"
                                            >
                                                <svg
                                                    width="16"
                                                    height="16"
                                                    viewBox="0 0 16 16"
                                                    fill="none"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                >
                                                    <path
                                                        d="M15 7.99971C15 8.41534 14.6657 8.74972 14.25 8.74972H9.25003V13.7497C9.25003 14.1638 8.91409 14.5 8.50002 14.5C8.08596 14.5 7.75002 14.1654 7.75002 13.7497V8.74972H2.75C2.33594 8.74972 2 8.41409 2 8.00003C2 7.58721 2.33594 7.24971 2.75 7.24971H7.75002V2.24969C7.75002 1.83563 8.08596 1.5 8.50002 1.5C8.91409 1.5 9.25003 1.83563 9.25003 2.24969V7.24971H14.25C14.6657 7.24971 15 7.58721 15 7.99971Z"
                                                        fill="#0052CC"
                                                    />
                                                </svg>

                                                <span>Add Keywords</span>
                                            </button>`;
                                }
                                return domData;
                            }

                            return data;
                        },
                    },
                    {
                        data: "status",
                        render: function (data, type) {
                            if (type === "display") {
                                let link;
                                if (data == 0) {
                                    link = ` <div
                                             class="badge-btn bg-r200 text-danger w-75 text-center"
                                            >
                                                Not started
                                            </div>`;
                                } else if (data == 3 || data == 4) {
                                    link = `<div

                                                class="badge-btn bg-g300 text-white w-75 text-center"
                                            >
                                                Published
                                            </div>`;
                                } else {
                                    link = `<div

                                                class="badge-btn bg-y300 text-white w-75 text-center"
                                            >
                                                Draft
                                            </div>`;
                                }

                                return link;
                            }
                            return data;
                        },
                    },
                    {
                        data: "focus_keyword",
                    },
                ],
                info: false,
                order: [[1, "desc"]],
                lengthChange: true,
                dom: '<"top"i>rt<"bottom"lp><"clear">',
                columnDefs: [{ responsivePriority: 2, targets: -1 }],
                dateFormat: "MMMM DD YYYY",
            });
            $("#searchArticles").on("keyup change clear", function (params) {
                sageWriterTable.search(params.target.value).draw();
            });
        },